(ns user
  (:require [mount.core :as mount]
            guestbook1.core))

(defn start []
  (mount/start-without #'guestbook1.core/repl-server))

(defn stop []
  (mount/stop-except #'guestbook1.core/repl-server))

(defn restart []
  (stop)
  (start))


