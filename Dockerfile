FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/guestbook1.jar /guestbook1/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/guestbook1/app.jar"]
