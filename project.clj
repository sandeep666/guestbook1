(defproject guestbook1 "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "http://example.com/FIXME"

  :dependencies [[bouncer "1.0.0"]
                 [ring "1.5.0"]
                 [com.h2database/h2 "1.4.192"]
                 [compojure "1.5.1"]
                 [conman "0.6.2"]
                 [cprop "0.1.9"]
                 [luminus-immutant "0.2.2"]
                 [luminus-migrations "0.2.8"]
                 [luminus-nrepl "0.1.4"]
                 [markdown-clj "0.9.90"]
                 [metosin/ring-http-response "0.8.0"]
                 [mount "0.1.10"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [org.clojure/tools.logging "0.3.1"]
                 [org.webjars.bower/tether "1.3.7"]
                 [org.webjars/bootstrap "4.0.0-alpha.5"]
                 [org.webjars/font-awesome "4.6.3"]
                 [org.webjars/jquery "3.1.1"]
                 [org.webjars/webjars-locator-jboss-vfs "0.1.0"]
                 [ring-middleware-format "0.7.0"]
                 [ring-webjars "0.1.1"]
                 [ring/ring-defaults "0.2.1"]
                 [selmer "1.10.0"]
                 [org.clojure/clojurescript "1.7.228" :scope "provided"]
                 [lein-figwheel "0.5.8"]
                 [reagent "0.5.1"]
                 [reagent-utils "0.2.0"]
                 [secretary "1.2.3"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.8.51"]
                 [cljs-ajax "0.5.8"]
                 [data-frisk-reagent "0.3.1"]
                 [reagent-forms "0.5.28"]]
  :ring {:handler guestbook1.core/wrapping-handler
         :init guestbook1.core/on-init
         :destroy guestbook1.core/on-destroy}

  :min-lein-version "2.0.0"

  :jvm-opts ["-server" "-Dconf=.lein-env"]
  :source-paths ["src/clj"]
  :resource-paths ["resources" "target/cljsbuild"]
  :cljsbuild
  {:builds
   {:app
    {:source-paths ["src/cljs"]
     :figwheel true
     :compiler
                   {:main          "guestbook1.core"
                    :asset-path    "/js/out"
                    :output-to     "target/cljsbuild/public/js/out/app.js"
                    :output-dir    "target/cljsbuild/public/js/out"
                    :optimizations :none
                    :source-map    true
                    :pretty-print  true}}

    }}
  :target-path "target/%s/"
  :main guestbook1.core
  :migratus {:store :database :db ~(get (System/getenv) "DATABASE_URL")}

  :plugins [[lein-cprop "1.0.1"]
            [migratus-lein "0.4.3"]
            [lein-immutant "2.1.0"]
            [lein-cljsbuild "1.1.3"]
            [lein-ring "0.8.7"]
            [lein-figwheel "0.5.4-7"] ]

  :profiles
  {:uberjar {:omit-source true
             :aot :all
             :uberjar-name "guestbook1.jar"
             :source-paths ["env/prod/clj"]
             :resource-paths ["env/prod/resources"]
             :prep-tasks ["compile" ["cljsbuild" "once" "min"]]}

   :dev           [:project/dev :profiles/dev]
   :test          [:project/dev :project/test :profiles/test]

   :project/dev  {:dependencies [[prone "1.1.2"]
                                 [ring/ring-mock "0.3.0"]
                                 [ring/ring-devel "1.5.0"]
                                 [pjstadig/humane-test-output "0.8.1"]]
                  :plugins      [[com.jakemccrary/lein-test-refresh "0.14.0"]]
                  
                  :source-paths ["env/dev/clj" "test/clj"]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns user}
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]
                  }


   :project/test {:resource-paths ["env/test/resources"]}
   :profiles/dev {}
   :profiles/test {}}
  :test-paths ["features" "test"]




  )


