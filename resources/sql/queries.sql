-- :name save-book! :! :n
-- :doc "Save a book"
INSERT INTO guestbook1
(bookname)
VALUES (:bookname)

-- :name select-books :? :*
-- :doc "Select all Books"
SELECT * FROM guestbook1



-- :name update-book :! :n
-- :doc "updates bookname"
UPDATE guestbook1
SET bookname = :newbookname
WHERE id = :id

-- :name delete-book! :! :n
-- :doc "delete a book"
DELETE FROM guestbook1
WHERE bookname = :bookname
AND id = :id

-- :name save-author! :! :n
-- :doc "Save a book"
INSERT INTO authors
(authorname)
VALUES (:authorname)


-- :name select-authors :? :*
-- :doc "Select all authors"
SELECT * FROM authors

-- :name update-author :! :n
-- :doc "updates author name"
UPDATE authors
SET authorname = :newauthorname
WHERE id = :id


-- :name delete-author! :! :n
-- :doc "delete a author name"
DELETE FROM authors
WHERE authorname = :authorname
AND id = :id

