// Compiled by ClojureScript 1.7.228 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4657__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4657__auto__)){
var ns = temp__4657__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__24200_24214 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__24201_24215 = null;
var count__24202_24216 = (0);
var i__24203_24217 = (0);
while(true){
if((i__24203_24217 < count__24202_24216)){
var f_24218 = cljs.core._nth.call(null,chunk__24201_24215,i__24203_24217);
cljs.core.println.call(null,"  ",f_24218);

var G__24219 = seq__24200_24214;
var G__24220 = chunk__24201_24215;
var G__24221 = count__24202_24216;
var G__24222 = (i__24203_24217 + (1));
seq__24200_24214 = G__24219;
chunk__24201_24215 = G__24220;
count__24202_24216 = G__24221;
i__24203_24217 = G__24222;
continue;
} else {
var temp__4657__auto___24223 = cljs.core.seq.call(null,seq__24200_24214);
if(temp__4657__auto___24223){
var seq__24200_24224__$1 = temp__4657__auto___24223;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24200_24224__$1)){
var c__21991__auto___24225 = cljs.core.chunk_first.call(null,seq__24200_24224__$1);
var G__24226 = cljs.core.chunk_rest.call(null,seq__24200_24224__$1);
var G__24227 = c__21991__auto___24225;
var G__24228 = cljs.core.count.call(null,c__21991__auto___24225);
var G__24229 = (0);
seq__24200_24214 = G__24226;
chunk__24201_24215 = G__24227;
count__24202_24216 = G__24228;
i__24203_24217 = G__24229;
continue;
} else {
var f_24230 = cljs.core.first.call(null,seq__24200_24224__$1);
cljs.core.println.call(null,"  ",f_24230);

var G__24231 = cljs.core.next.call(null,seq__24200_24224__$1);
var G__24232 = null;
var G__24233 = (0);
var G__24234 = (0);
seq__24200_24214 = G__24231;
chunk__24201_24215 = G__24232;
count__24202_24216 = G__24233;
i__24203_24217 = G__24234;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_24235 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__21188__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__21188__auto__)){
return or__21188__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_24235);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_24235)))?cljs.core.second.call(null,arglists_24235):arglists_24235));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__24204 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__24205 = null;
var count__24206 = (0);
var i__24207 = (0);
while(true){
if((i__24207 < count__24206)){
var vec__24208 = cljs.core._nth.call(null,chunk__24205,i__24207);
var name = cljs.core.nth.call(null,vec__24208,(0),null);
var map__24209 = cljs.core.nth.call(null,vec__24208,(1),null);
var map__24209__$1 = ((((!((map__24209 == null)))?((((map__24209.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24209.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24209):map__24209);
var doc = cljs.core.get.call(null,map__24209__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__24209__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__24236 = seq__24204;
var G__24237 = chunk__24205;
var G__24238 = count__24206;
var G__24239 = (i__24207 + (1));
seq__24204 = G__24236;
chunk__24205 = G__24237;
count__24206 = G__24238;
i__24207 = G__24239;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__24204);
if(temp__4657__auto__){
var seq__24204__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24204__$1)){
var c__21991__auto__ = cljs.core.chunk_first.call(null,seq__24204__$1);
var G__24240 = cljs.core.chunk_rest.call(null,seq__24204__$1);
var G__24241 = c__21991__auto__;
var G__24242 = cljs.core.count.call(null,c__21991__auto__);
var G__24243 = (0);
seq__24204 = G__24240;
chunk__24205 = G__24241;
count__24206 = G__24242;
i__24207 = G__24243;
continue;
} else {
var vec__24211 = cljs.core.first.call(null,seq__24204__$1);
var name = cljs.core.nth.call(null,vec__24211,(0),null);
var map__24212 = cljs.core.nth.call(null,vec__24211,(1),null);
var map__24212__$1 = ((((!((map__24212 == null)))?((((map__24212.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24212.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24212):map__24212);
var doc = cljs.core.get.call(null,map__24212__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__24212__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__24244 = cljs.core.next.call(null,seq__24204__$1);
var G__24245 = null;
var G__24246 = (0);
var G__24247 = (0);
seq__24204 = G__24244;
chunk__24205 = G__24245;
count__24206 = G__24246;
i__24207 = G__24247;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map