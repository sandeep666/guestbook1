(ns guestbook1.core
  (:require [reagent.core :as r]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [secretary.core :as secretary :refer [dispatch!] :include-macros true]
            [reagent.session :as session]
            [ajax.core :refer [GET POST PUT DELETE]]
            [datafrisk.core :as d])
  (:import goog.History))

(def app-db (r/atom {}))


(defn book-list []
  (do (GET "/books" {:handler #(swap! app-db update-in [:book-list] (fn [] %))})
      (GET "/authors" {:handler #(swap! app-db update-in [:author-list] (fn [] %))}))
  (fn []
    [:div.form-group
     [:h3 "book list"]
      [:label "pick a book"]
      [:select {:on-change #(swap! app-db update-in [:current-book] (fn []
                                                                      (first (filter
                                                                               (fn [book] (= (str (:id book))
                                                                                             (-> % .-target .-value)))
                                                                               (:book-list @app-db)))))}
       (for [book (:book-list @app-db)]
         ^{:key (:id book)}[:option {:value (:id book)} (:bookname book)])
  [:input {:type     "button"
           :value    "go"
           :on-click #(dispatch! "/view-book")}]]


      [:input {:type     "button"
               :value    "Add book"
               :on-click #(dispatch! "/add-book")}]


      [:p "list of authors"

       [:input {:type     "button"
                :value    "Add author"
                :on-click #(dispatch! "/add-author")}]]

      [:ul
       (for [author (:author-list @app-db)]
         ^{:key author} [:li [:input {:type     "button" :value (:authorname author)
                                      :on-click #(do (swap! app-db update-in [:current-author] (fn [] author))
                                                     (dispatch! "/view-author"))}]])]

      ]))


(defn add-book []
  [:div
   [:p "Book name"
    [:input {:type "text" :on-change #(swap! app-db update-in [:current-book :bookname]
                                             (fn [] (-> % .-target .-value)))}]]
   [:input {:type "button" :value "back" :on-click #(dispatch! "/") }]
   [:input {:type     "button" :value "Save"
            :on-click #(POST "/book" {:params  ( :current-book @app-db)
                                      :handler (fn [response] (dispatch! "/"))})}]])


(defn add-author []
  [:div
   [:p "author name"
    [:input {:type "text" :on-change #(swap! app-db update-in [:current-author :authorname]
                                             (fn [] (-> % .-target .-value)))}]]
   [:input {:type "button" :value "back" :on-click #(dispatch! "/") }]
   [:input {:type     "button" :value "Save"
            :on-click #(POST "/author" {:params  ( :current-author @app-db)
                                      :handler (fn [response] (dispatch! "/"))})}]]
   )



(defn book-page []
  (fn []
    [:div
     [:p "book name : " (-> @app-db :current-book :bookname)]
     [:p "book ID : " (-> @app-db :current-book :id)]
     [:input {:type     "button"
              :value    "back"
              :on-click #(dispatch! "/")}]

     [:input {:type     "button" :value " update"
              :on-click #(do (dispatch! "/update-book")
                          )}]
     [:input {:type "button" :value "delete" :on-click #(DELETE "/delete-book"{:params  (:current-book @app-db)
                                                                               :handler (fn [response] (dispatch! "/"))} ) }]]))


(defn update-book []
  [:div
   [:p "old book name :"(-> @app-db :current-book :bookname) ]
   [:p " new name" ]
   [:input {:type "text"
            :on-change #(do  (swap! app-db update-in [:current-book :newbookname]
                                   (fn [] (-> % .-target .-value))) ) }
    [:input {:type "button"
             :value "update"
             :on-click #(PUT "/update-book" {:params  (:current-book @app-db)
                                             :handler (fn [response] (dispatch! "/"))} )}]]])

(defn author-page []
  (fn []
    [:div
     [:p "author name : " (-> @app-db :current-author :authorname)]
     [:input {:type     "button"
              :value    "back"
              :on-click #(dispatch! "/")}]

     [:input {:type     "button" :value " update"
              :on-click #(do (dispatch! "/update-author")
                             )}]
     [:input {:type "button" :value "delete" :on-click #(DELETE "/delete-author" {:params  (:current-author @app-db)
                                                                                :handler (fn [response] (dispatch! "/"))})}]]))

(defn update-author []
  [:div
   [:p "old author name :"(-> @app-db :current-author :authorname) ]
   [:p " new name" ]
   [:input {:type "text"
            :on-change #(do  (swap! app-db update-in [:current-author :newauthorname]
                                    (fn [] (-> % .-target .-value))) ) }
    [:input {:type "button"
             :value "update"
             :on-click #(PUT "/update-author" {:params  (:current-author @app-db)
                                             :handler (fn [response] (dispatch! "/"))} )}]]])





(defn page []
  [:div
   [(session/get :current-page)]
   [d/DataFriskShell @app-db]])

(secretary/set-config! :prefix "#")


(secretary/defroute "/" []
                    (session/put! :current-page book-list))

(secretary/defroute "/view-book" []
                    (session/put! :current-page book-page))

(secretary/defroute "/view-author" []
                    (session/put! :current-page author-page))

(secretary/defroute "/add-book" []
                    (session/put! :current-page add-book))

(secretary/defroute "/add-author" []
                    (session/put! :current-page add-author))

(secretary/defroute "/update-book" []
                    (session/put! :current-page update-book))

(secretary/defroute "/update-author" []
                    (session/put! :current-page update-author))


(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      HistoryEventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn mount-components []
  (r/render [#'page] (.getElementById js/document "content")))

(defn init! []
  (hook-browser-navigation!)
  (mount-components))

(init!)