(ns guestbook1.routes.home
  (:require [guestbook1.layout :as layout]
            [compojure.core :refer [defroutes GET POST PUT DELETE]]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [guestbook1.db.core :as db]
            ))


(defroutes home-routes
           (GET "/" [] (layout/render "home.html"))
           (GET "/books" [] (db/select-books))
           (GET "/authors" [] (db/select-authors))
           (POST "/book" [] (fn [request] {:response (db/save-book! (:params request))}))
           (POST "/author" [] (fn [request] {:response (db/save-author! (:params request))}))
           (PUT "/update-book" [] (fn [request] {:response (db/update-book (:params request))}))
           (PUT "/update-author" [] (fn [request] {:response (db/update-author (:params request))}))
           (DELETE "/delete-book" [] (fn [request] {:response (db/delete-book! (:params request))}) ))
           (DELETE "/delete-author" [] (fn [request] {:response (db/delete-author! (:params request))}) )


